# MasterPodelkin site with WP

CSS files are compiling from .styl format.
You need to change source code in /wp-content/themes/masterpodelkin1/assets directory with gulp.js

## Running gulp.js for frontend
[*Nodejs and npm*](http://nodejs.org/) must be installed
1. Go to directory /wp-content/themes/masterpodelkin1/
2. Type
```
npm i -g gulp
npm i
```
3. To run task manager type
```
gulp
```
4. Change files in /wp-content/themes/masterpodelkin1/assets/styl directory
5. Look for /wp-content/themes/masterpodelkin1/assets/css directory

## Access
login: masterpodelkin
password: _masterpodelkin!

## SQL
masterpodelkin_wp.sql