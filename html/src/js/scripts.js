$(function() {

  /**
   * Sidebar events
   */
  $('.sidebar-left').on('click', '.more-categories', function() {
    var $this = $(this);
    var text = $(this).find('.text').text() == 'показать все' ? 'скрыть все' : 'показать все';
    var arrtext = $(this).find('.arr').html() == '&darr;' ? '&darr;' : '&uarr;';
    $this.find('.text').text(text).
      end().find('.arr').html(arrtext).
      end().parent().find('ul').toggleClass('open');
  });


});