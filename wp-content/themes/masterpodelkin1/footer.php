<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the wrapper div and all content after
 *
 * @package masterpodelkin1
 */
?>

    <?php $options = get_option('podelkin_theme_options'); ?>

    <footer id="footer" class="clearfix">
      <div class="container">
        <div class="footer-top row">
          <div class="menu-section col-xs-6 col-sm-12 col-md-9">
            <div class="row">
              <nav class="col-sm-3">
                <h5><?php get_menu_title('footer_menu1') ?></h5>
                <?php
                  wp_nav_menu( array(
                    'theme_location'    => 'footer_menu1',
                    'container'         => '',)
                  );
                ?>
              </nav>
              <nav class="col-sm-3">
                <h5><?php get_menu_title('footer_menu2') ?></h5>
                <?php
                  wp_nav_menu( array(
                    'theme_location'    => 'footer_menu2',
                    'container'         => '',)
                  );
                ?>
              </nav>
              <nav class="col-sm-6 events-nav">
                <h5>Мероприятия </h5>

                <?php
                  $args = array(
                    'parent'  => 25,
                    'orderby' => 'term_order',
                  );
                  $categories = get_categories( $args );
                  $i = 1;
                  if ( ! empty($categories) ) : ?>
                    <?php foreach($categories as $category) : ?>
                      <?php if ($i == 1 || $i % 5 == 1) : ?>
                        <ul>
                      <?php endif; ?>
                          <li><a href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?></a></li>
                      <?php if ($i % 5 == 0 ) :?>
                        </ul>
                      <?php endif; ?>
                      <?php $i++; ?>
                    <?php endforeach; ?>

                <?php endif; ?>


                <?php //get_menu_title('footer_menu3') ?>


                 <?php
                  /*wp_nav_menu( array(
                    'theme_location'    => 'footer_menu3',
                    'container'         => '',)
                  );
                  wp_nav_menu( array(
                    'theme_location'    => 'footer_menu3',
                    'container'         => '',)
                  );*/
                ?>
              </nav>
            </div>
          </div><!-- /.menu-section -->

          <div class="soc-links col-xs-6 col-sm-12 col-md-3">
            <div class="soc-links-section">
              <h5>Мы в сетях:</h5>
              <a href="<?php echo $options['soc_vk']; ?>" target="_blank"><i class="sprite sprite-soc-vk"></i></a>
              <a href="<?php echo $options['soc_fb']; ?>" target="_blank"><i class="sprite sprite-soc-fb"></i></a>
              <a href="<?php echo $options['soc_youtube']; ?>" target="_blank"><i class="sprite sprite-soc-youtube"></i></a>
              <a href="<?php echo $options['soc_instagram']; ?>" target="_blank"><i class="sprite sprite-soc-instagram"></i></a>
              <a href="<?php echo $options['soc_ok']; ?>" target="_blank"><i class="sprite sprite-soc-ok"></i></a>
              <a href="<?php echo $options['soc_forsquare']; ?>" target="_blank"><i class="sprite sprite-soc-forsquare"></i></a>
            </div>
            <div class="info-block info-instagram phone-block">
              <div class="info-text">
                <a href="tel:<?php echo $options['phone']; ?>" class="phone-link"><?php echo $options['phone']; ?></a>
                <a href="tel:<?php echo $options['phone_viber']; ?>" class="phone-link"><?php echo $options['phone_viber']; ?></a>
                <a href="mailto:mail@masterpodelkin.ru">mail@masterpodelkin.ru</a>
              </div>
            </div>
          </div><!-- /.soc-links -->
        </div>
      </div>


      <div class="metrics clearfix">
        <!-- Yandex.Metrika informer -->
        <a href="https://metrika.yandex.ru/stat/?id=29011710&amp;from=informer"
        target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/29011710/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
        style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:29011710,lang:'ru'});return false}catch(e){}" /></a>
        <!-- /Yandex.Metrika informer -->
        <script type="text/javascript">
        document.write("<a href='http://www.liveinternet.ru/click' "+
        "target=_blank><img src='//counter.yadro.ru/hit?t22.6;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";"+Math.random()+
        "' alt='' title='LiveInternet: показано число просмотров за 24"+
        " часа, посетителей за 24 часа и за сегодня' "+
        "border='0' width='88' height='31'><\/a>")
        //--></script><!--/LiveInternet--><!-- Счётчик профессиональной event сети PartyInfo.ru --><a href="http://partyinfo.ru/"><img src="http://partyinfo.ru/counter=8526e0962a844e4a2f158d831d5fddf7" alt="Каталог артистов - Готовые программы" width="88" height="31"></a><!-- / Счётчик профессиональной event сети PartyInfo.ru --><a title="База Артистов - контакты всех артистов, фото и видео артистов" href="http://www.baza-artistov.ru/"><img src="http://www.baza-artistov.ru/img/88x31.gif" border="0" alt="" width="88" height="31"><!-- EventCatalog.ru widget start -->
        <div class="evcInformer" style="float:left;" id="eventcatalogWidget" data-widget-type="2" data-widget-hash="db7597ae34bca3f64659dd2e8cdd9d94" data-resident-id="6921-c"><div class="evcWrapper"><div class="evcRatingWrapper"><a href="http://eventcatalog.ru" title="EventCatalog.ru"><img id="evcWidgetImage" class="evcWidgetImage" src="http://eventcatalog.ru/images/widgets/widget-type-2.png" alt="eventcatalog widget"></a><script src="http://eventcatalog.ru/js/widget.js" type="text/javascript"></script></div></div></div>
        <!-- EventCatalog.ru widget end -->


        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
          (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
              try {
                  w.yaCounter29011710 = new Ya.Metrika({
                      id:29011710,
                      clickmap:true,
                      trackLinks:true,
                      accurateTrackBounce:true,
                      webvisor:true
                  });
              } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
              d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
          })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/29011710" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

      </div>

    </footer>

  </div><!-- /.wrapper -->


  <div class="modal fade" id="callback_modal">
    <div class="modal-dialog">
      <button title="Close" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
      <div class="modal-content">
        <h2>Заказать звонок</h2>
        <?php echo do_shortcode('[contact-form-7 id="78" title="Заказать звонок"]') ?>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div class="modal fade" id="contact_modal">
    <div class="modal-dialog">
      <button title="Close" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
      <div class="modal-content">
        <h2>Написать нам</h2>
        <?php echo do_shortcode('[contact-form-7 id="264" title="Написать нам"]') ?>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <?php wp_footer(); ?>
</body>
</html>