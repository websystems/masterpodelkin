<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
  register_setting( 'podelkin_options', 'podelkin_theme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
  add_theme_page( __( 'Настройки Поделкина', 'masterpodelkin1' ), __( 'Настройки Поделкина', 'masterpodelkin1' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
 * Create the options page
 */
function theme_options_do_page() {

  if ( ! isset( $_REQUEST['settings-updated'] ) )
    $_REQUEST['settings-updated'] = false;

  ?>
  <div class="wrap">
    <?php screen_icon(); echo "<h2>" . __( 'Настройки Поделкина', 'masterpodelkin1' ) . "</h2>"; ?>

    <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
    <div class="updated fade"><p><strong><?php _e( 'Настройки сохранены', 'masterpodelkin1' ); ?></strong></p></div>
    <?php endif; ?>

    <form method="post" action="options.php">
      <?php settings_fields( 'podelkin_options' ); ?>
      <?php $options = get_option( 'podelkin_theme_options' ); ?>

      <table class="form-table">

<!--         <?php
        /**
         * A checkbox option
         */
        ?>
        <tr valign="top"><th scope="row"><?php _e( 'Описание в футере', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[option1]" name="podelkin_theme_options[option1]" type="checkbox" value="1" <?php checked( '1', $options['option1'] ); ?> />
            <label class="description" for="podelkin_theme_options[option1]"><?php _e( 'Sample checkbox', 'masterpodelkin1' ); ?></label>
          </td>
        </tr> -->

        <?php
        /**
         * A text input option
         */
        ?>
        <tr>
          <td>
            <strong>Настройки хедера</strong>
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Адрес', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[address]" class="regular-text" type="text" name="podelkin_theme_options[address]" value="<?php esc_attr_e( $options['address'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Телефон', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[phone]" class="regular-text" type="text" name="podelkin_theme_options[phone]" value="<?php esc_attr_e( $options['phone'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Телефон (viber)', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[phone_viber]" class="regular-text" type="text" name="podelkin_theme_options[phone_viber]" value="<?php esc_attr_e( $options['phone_viber'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Время работы', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[worktime]" class="regular-text" type="text" name="podelkin_theme_options[worktime]" value="<?php esc_attr_e( $options['worktime'] ); ?>" />
          </td>
        </tr>


        <tr>
          <td>
            <strong>Ссылки на соц. сети</strong>
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Вконтакте', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[soc_vk]" class="regular-text" type="text" name="podelkin_theme_options[soc_vk]" value="<?php esc_attr_e( $options['soc_vk'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Facebook', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[soc_fb]" class="regular-text" type="text" name="podelkin_theme_options[soc_fb]" value="<?php esc_attr_e( $options['soc_fb'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Youtube', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[soc_youtube]" class="regular-text" type="text" name="podelkin_theme_options[soc_youtube]" value="<?php esc_attr_e( $options['soc_youtube'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Instagram', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[instagram]" class="regular-text" type="text" name="podelkin_theme_options[instagram]" value="<?php esc_attr_e( $options['instagram'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Одноклассники', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[soc_ok]" class="regular-text" type="text" name="podelkin_theme_options[soc_ok]" value="<?php esc_attr_e( $options['soc_ok'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Forsquare', 'masterpodelkin1' ); ?></th>
          <td>
            <input id="podelkin_theme_options[soc_forsquare]" class="regular-text" type="text" name="podelkin_theme_options[soc_forsquare]" value="<?php esc_attr_e( $options['soc_forsquare'] ); ?>" />
          </td>
        </tr>

        <?php
        /**
         * A textarea option
         */
        ?>
        <tr valign="top"><th scope="row"><?php _e( 'Описание в футере', 'masterpodelkin1' ); ?></th>
          <td>
            <textarea id="podelkin_theme_options[about]" class="large-text" cols="50" rows="10" name="podelkin_theme_options[about]"><?php echo esc_textarea( $options['about'] ); ?></textarea>
          </td>
        </tr>
      </table>

      <p class="submit">
        <input type="submit" class="button-primary" value="<?php _e( 'Сохранить настройки', 'masterpodelkin1' ); ?>" />
      </p>
    </form>
  </div>
  <?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
  // Our checkbox value is either 0 or 1
  // if ( ! isset( $input['option1'] ) )
  //   $input['option1'] = null;
  // $input['option1'] = ( $input['option1'] == 1 ? 1 : 0 );

  // Say our text option must be safe text with no HTML tags
  $input['address'] = wp_filter_nohtml_kses( $input['address'] );
  $input['phone'] = wp_filter_nohtml_kses( $input['phone'] );
  $input['phon_viber'] = wp_filter_nohtml_kses( $input['phone_viber'] );
  $input['worktime'] = wp_filter_nohtml_kses( $input['worktime'] );
  $input['about'] = wp_filter_post_kses( $input['about'] );

  return $input;
}


