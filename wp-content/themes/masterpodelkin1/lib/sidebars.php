<?php
/**
 * Sidebars
 */
if ( function_exists('register_sidebar') )
  if ( function_exists('register_sidebar') )
    register_sidebar(array('name'=>'Left Sidebar', //Name your sidebar
      'description' => 'These widgets will appear in the left sidebar.',
  ));

if ( function_exists('register_sidebar') )
  if ( function_exists('register_sidebar') )
    register_sidebar(array('name'=>'Right Sidebar', //Name your sidebar
      'description' => 'These widgets will appear in the right sidebar.',
  ));



/**
 * Download files widget
 */
class Download_widget extends WP_Widget {

  function __construct() {
  parent::__construct(
    // Base ID of your widget
    'download_widget',

    // Widget name will appear in UI
    __('Скачать файлы', 'download_widget_domain'),

    // Widget description
    array( 'description' => __( 'Виджет для скачивания файлов', 'download_widget_domain' ), )
    );
  }

  // Creating widget front-end
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    $link = apply_filters( 'widget_title', $instance['link'] );
    // before and after widget arguments are defined by themes
    if ( !empty($title) && !empty($link) ) {
      echo '<a href="' . $link . '" class="btn btn-callback btn-download" data-toggle="modal" download>' . $title .'</a>';
    }
  }

  // Widget Backend
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    } else {
      $title = __( 'Скачать', 'download_widget_domain' );
    }
    if ( isset( $instance[ 'link' ] ) ) {
      $link = $instance[ 'link' ];
    }

    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Название:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <p>
    <label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Ссылка:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
    </p>
    <p>
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['link'] = ( ! empty( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : '';
    return $instance;
  }
}

// Register and load the widget
function download_load_widget() {
  register_widget( 'download_widget' );
}
add_action( 'widgets_init', 'download_load_widget' );