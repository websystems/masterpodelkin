<?php
  /**
   * Enqueue scripts and styles.
   */
  function masterpodelkin1_scripts() {

    // bootstrap assets
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array('jquery'), '1.0.0', true );

    // custom assets
    wp_enqueue_style( 'custom', get_template_directory_uri() . '/assets/css/style.min.css', array('bootstrap') );
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery', 'bootstrap'), '1.0.0', true );

  }

  add_action( 'wp_enqueue_scripts', 'masterpodelkin1_scripts' );