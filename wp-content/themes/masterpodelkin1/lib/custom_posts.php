<?php

/**
 * Master classes
 */
function master_classes_register() {
    $labels = array(
        'name' => _x('Мастер-классы', 'post type general name'),
        'singular_name' => _x('Мастер-класс Item', 'post type singular name'),
        'add_new' => _x('Добавить новый', 'portfolio item'),
        'add_new_item' => __('Добавить новый Мастер-класс'),
        'edit_item' => __('Редактировать Мастер-класс'),
        'new_item' => __('Новый Мастер-класс'),
        'view_item' => __('Список Мастер-классов'),
        'search_items' => __('Поиск по Мастер-класам'),
        'not_found' =>  __('Ничего не найдено'),
        'not_found_in_trash' => __('Удаленные Масте-классы'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail', 'excerpt')
    );
    register_post_type( 'master_klasses' , $args );
}
add_action('init', 'master_classes_register');


function create_master_classes_taxonomy() {
    $labels = array(
        'name'              => _x( 'Категории', 'taxonomy general name' ),
        'singular_name'     => _x( 'Категория', 'taxonomy singular name' ),
        'menu_name'         => __( 'Категории Мастер-классов' ),
        'search_items'      => __( 'Поиск по категориям' ),
        'all_items'         => __( 'Все категории' ),
        'parent_item'       => __( 'Родительская категория' ),
        'parent_item_colon' => __( 'Родительская категория:' ),
        'edit_item'         => __( 'Изменить категорию' ),
        'update_item'       => __( 'Обновить категорию' ),
        'add_new_item'      => __( 'Добавить новую категорию' ),
        'new_item_name'     => __( 'Создать новую категорию' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );

    register_taxonomy( 'master_classes_categories', array( 'master_klasses' ), $args );
}
add_action( 'init', 'create_master_classes_taxonomy', 0 );


