<?php

/**
 * Thumbnails for post
 */
add_theme_support( 'post-thumbnails' );


/**
 * Russian date
 */
function rus_date() {
  $months = array(
                  'января',
                  'февраля',
                  'марта',
                  'апреля',
                  'мая',
                  'июня',
                  'июля',
                  'августа',
                  'сентября',
                  'октрября',
                  'ноября',
                  'декабря'
              );

  $date = get_the_date('j-n-Y');

  $date = explode('-', $date);
  $date[1] = $months[$date[1]-1];

  echo implode(' ', $date);
}


if ( ! function_exists( 'numeric_posts_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 * Based on paging nav function from Twenty Fourteen
 */

function numeric_posts_nav($custom_post = null) {
  // Don't print empty markup if there's only one page.

  if ( $GLOBALS['wp_query']->max_num_pages < 2 && $GLOBALS['wpex_query']->max_num_pages < 2 ) {
    return;
  }

  if ($GLOBALS['wpex_query']) {
    $total = $GLOBALS['wpex_query']->max_num_pages;
  } else {
    $total = $GLOBALS['wp_query']->max_num_pages;
  }

  $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
  $pagenum_link = html_entity_decode( get_pagenum_link() );
  $query_args   = array();
  $url_parts    = explode( '?', $pagenum_link );

  if ( isset( $url_parts[1] ) ) {
    wp_parse_str( $url_parts[1], $query_args );
  }

  $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
  $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

  $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';


  if ($GLOBALS['wpex_query']) {
    $tmp = explode('/', $pagenum_link);
    if ($tmp[3] == trim($custom_post, '/'))
      $custom_post = '';
  }

  $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( $custom_post.'page/%#%', 'paged' ) : '?paged=%#%';

  // Set up paginated links.
  $links = paginate_links( array(
    'base'     => $pagenum_link,
    'format'   => $format,
    'total'    => $total,
    'current'  => $paged,
    'mid_size' => 3,
    'add_args' => array_map( 'urlencode', $query_args ),
    'prev_text' => __( '<i class="sprite sprite-arrow-left"></i>', 'masterpodelnkin1' ),
    'next_text' => __( '<i class="sprite sprite-arrow-right"></i>', 'masterpodelnkin1' ),
    'type'      => 'list',
  ) );

  if ( $links ) :

  ?>
  <nav class="paginator paging-navigation" role="navigation">
      <?php echo $links; ?>
  </nav><!-- .navigation -->
  <?php
  endif;
}
endif;



/**
 * Removing "read more"
 */
function new_excerpt_more( $more ) {
  global $post;
  if ($post->post_type == 'testimonial'){
    return '';
  }
}
add_filter('excerpt_more', 'new_excerpt_more');
