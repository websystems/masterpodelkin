<?php

/**
* Add menus to site
*/
register_nav_menus( array(
  'primary' => __( 'Primary Menu', 'masterpodelkin1' ),
  'footer_menu1' => __('Footer Menu 1', 'masterpodelkin1'),
  'footer_menu2' => __('Footer Menu 2', 'masterpodelkin1'),
  // 'footer_menu3' => __('Footer Menu 3', 'masterpodelkin1'),
) );

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

/**
 * Prints menu name
 *
 * @param string menu_id
 */
function get_menu_title($nav_id) {
  $menu_locations = (array) get_nav_menu_locations();
  // In the line below change "primary" to your menu ID (the 'theme_location' parameter in wp_nav_menu())
  $menu = get_term_by( 'id', (int) $menu_locations[ $nav_id ], 'nav_menu', ARRAY_A );
  echo $menu['name'];
}