<aside class="sidebar-right">

  <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Right sidebar') ) : ?>
  <?php endif; ?>

  <div class="categories">
    <?php
    $args = array(
      'parent'  => 25,
      'orderby' => 'term_order',
    );
    $categories = get_categories( $args );
    if ( ! empty($categories) ) : ?>
      <div class="category-section full-height">
        <h2>Мероприятия</h2>
        <ul>
          <?php foreach($categories as $category) : ?>
            <li><a href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?> (<?php echo $category->count; ?>)</a></li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php endif; ?>

    <div class="category-section full-height">
      <h2>Новости</h2>
      <ul>
         <?php
          $query_args = array(
            'category_name' => 'news',
            'posts_per_page' => 5,
            );
          $news = new WP_Query($query_args);
        ?>
        <?php if ( $news->have_posts() ) : ?>
          <?php while ( $news->have_posts() ) : $news->the_post();  ?>
            <li><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></li>
          <?php endwhile; ?>
        <?php endif; ?>
        <li class="view-all"><a href="<?php echo get_page_link(8); ?>">посмотреть все</a></li>
      </ul>
    </div>
  </div><!--  /.categories -->

  <div class="soc-section">

    <div class="instagram-block">
      <div class="instagram-header">
        <i class="sprite sprite-soc-instagram"></i>Мы в Instagram:
      </div>
      <?php echo do_shortcode('[instagram-feed]'); ?>
    </div>

  </div><!-- /.soc-section -->
</aside><!-- /.sidebar-right -->