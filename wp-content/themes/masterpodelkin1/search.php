<?php
/**
 * The template for search results page.
 *
 * Main page
 *
 * @package masterpodelkin1
 */
  get_header();
 ?>

<div class="main-container">

  <div class="container">

    <?php get_sidebar('left') ?>


    <main class="main-section">
      <?php if ( have_posts() ) : ?>

        <h3 class="page-title"><?php printf( __( 'Результаты поиска для: %s', 'masterpodelkin1' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
        <div class="news-section">
          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'templates/content', 'search' ); ?>
          <?php endwhile; ?>
        </div>
      <?php else : ?>

        <h4><?php printf( __( 'Извините, ничего не найдено.', 'masterpodelkin1' )) ?></h4>

      <?php endif; ?>

    </main><!--/.main-section -->


    <?php get_sidebar('right') ?>

  </div><!-- /.container -->
</div><!-- /.main-container -->


<?php get_footer(); ?>
