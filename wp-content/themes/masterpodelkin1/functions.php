<?php

require_once('lib/include_scripts.php');
require_once('lib/navigation.php');
require_once('lib/posts.php');
require_once('lib/settings.php');
require_once('lib/custom_posts.php');
require_once('lib/sidebars.php');


add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;

function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="video-container">'.$html.'</div>';
    return $return;
}