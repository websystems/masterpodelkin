jQuery(function($) {

  /**
   * Sidebar events
   */
  $('.sidebar-left, .sidebar-right').on('click', '.more-categories', function(e) {
    e.preventDefault();
    var $this = $(this);
    var text = $(this).find('.text').text() == 'показать все' ? 'скрыть все' : 'показать все';
    var arrtext = $(this).find('.arr').html() == '&darr;' ? '&darr;' : '&uarr;';
    $this.find('.text').text(text).
      end().find('.arr').html(arrtext).
      end().parent().find('ul').toggleClass('open');
  });

  /**
   * Get current category dummycode
   */
  if ($('#current_category').length) {
    var $hiddenCategory = $('#current_category');
    var val = $hiddenCategory.text();
    $('.category-list .' + val).addClass('current-category');
  }

});