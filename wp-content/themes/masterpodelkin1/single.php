<?php
/**
 * The template for displaying the footer.
 *
 * Main page
 *
 * @package masterpodelkin1
 */
  get_header();
 ?>

<div class="main-container">

  <div class="container">

    <?php get_sidebar('left') ?>


    <main class="main-section post-section">
      <?php the_post(); ?>
      <h3 class="page-title"><?php the_title(); ?></h3>
      <div class="page-content">
        <?php the_content(); ?>
      </div>

    </main><!--/.main-section -->


    <?php get_sidebar('right') ?>

  </div><!-- /.container -->
</div><!-- /.main-container -->

<?php get_footer(); ?>