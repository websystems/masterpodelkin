<?php
/**
 * Template Name: Новости
 *
 * @package masterpodelkin1
 */

  get_header();
 ?>

<div class="main-container">

  <div class="container">

    <?php get_sidebar('left') ?>

    <?php wp_reset_query(); ?>
    <main class="main-section">
      <?php
/*      $args = array(
        'show_option_all'    => '',
        'orderby'            => 'date',
        'order'              => 'ASC',
        'style'              => 'list',
        'show_count'         => 0,
        'hide_empty'         => 1,
        'use_desc_for_title' => 1,
        'child_of'           => 0,
        'feed'               => '',
        'feed_type'          => '',
        'feed_image'         => '',
        'exclude'            => '',
        'exclude_tree'       => '',
        'include'            => '',
        'hierarchical'       => 1,
        'title_li'           => __( '' ),
        'show_option_none'   => __( '' ),
        'number'             => null,
        'echo'               => 1,
        'depth'              => 0,
        'current_category'   => 1,
        'pad_counts'         => 0,
      );*/
      ?>
      <h3 class="page-title stick-top">Новости</h3>
      <div class="news-section">
        <?php
          $query_args = array(
            'category_name' => 'news',
            'posts_per_page' => 5,
            'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
            );
          $posts = query_posts($query_args);
        ?>
        <?php if ( have_posts() ) : ?>

          <!-- Start of the main loop. -->
          <?php while ( have_posts() ) : the_post();  ?>
            <?php get_template_part('templates/content', get_post_format()); ?>

          <!-- the rest of your theme's main loop -->

          <?php endwhile; ?>
          <!-- End of the main loop -->

          <!-- Add the pagination functions here. -->
          <?php numeric_posts_nav(); ?>

          <?php else : ?>
          <p><?php _e('Извините, записей пока нет.', 'masterpodelkin1'); ?></p>
        <?php endif; ?>

      </div><!-- /.news-section -->


    </main><!--/.main-section -->


    <?php get_sidebar('right') ?>

  </div><!-- /.container -->
</div><!-- /.main-container -->

<?php get_footer(); ?>