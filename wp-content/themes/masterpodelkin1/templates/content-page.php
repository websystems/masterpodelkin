<div class="news-item clearfix">
  <?php if ( has_post_thumbnail() ) { ?>
    <a href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail('thumbnail', array('class' => 'news-thumb')); ?>
    </a>
  <?php } ?>
  <div class="news-content">
    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
    <div class="news-text">
      <?php the_excerpt(); ?>
    </div>
    <div class="date"><?php rus_date(); ?></div>
  </div>
</div><!-- /.news-item -->