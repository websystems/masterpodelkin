<?php
/**
 * The template for index page
 *
 * Main page
 *
 * @package masterpodelkin1
 */
  get_header();
 ?>

<div class="main-container">

  <div class="container">

    <?php get_sidebar('left') ?>


    <main class="main-section">

      <?php if (is_category()): ?>
        <h4 class="page-title"><?php single_cat_title( '', true ); ?></h4>
      <?php elseif (is_home()): ?>
        <h3 class="page-title">Лента событий</h3>
      <?php endif; ?>
      <div class="news-section">

        <?php if ( have_posts() ) : ?>

          <?php while ( have_posts() ) : the_post();  ?>
            <?php get_template_part('templates/content', get_post_format()); ?>

          <?php endwhile; ?>

          <?php numeric_posts_nav(); ?>

          <?php else : ?>
          <p><?php _e('Извините, записей пока нет.', 'masterpodelkin1'); ?></p>
        <?php endif; ?>

      </div><!-- /.news-section -->


    </main><!--/.main-section -->


    <?php get_sidebar('right') ?>

  </div><!-- /.container -->
</div><!-- /.main-container -->

<?php get_footer(); ?>