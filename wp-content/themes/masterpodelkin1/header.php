<?php
/**
 * The template for displaying the footer.
 *
 * Contains the head, #header and opening div .wrapper
 *
 * @package masterpodelkin1
 */
 ?>
<!DOCTYPE html>
<html>

<head <?php language_attributes(); ?>>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <?php wp_head(); ?>
</head>

<body>
  <div id="fb-root"></div>

  <?php $options = get_option('podelkin_theme_options'); ?>

  <header id="header">
    <div class="container">
      <div class="header-top row">

            <div class="col-sm-6 col-md-3">
              <a href="<?php echo home_url() ?>" class="logo">
                <img src="<?php echo get_template_directory_uri() ?>/assets//img/logo.png" class="img-responsive">
                <h4>Творческая студия</h4>
              </a>
            </div>

            <div class="col-sm-6 col-md-push-6 col-md-3">
              <div class="info-block info-instagram phone-block"><i class="sprite sprite-messanger"></i>
                <div class="info-text">
                  <a href="tel:<?php echo $options['phone_viber']; ?>" class="phone-link"><?php echo $options['phone_viber']; ?></a>
                  <small>SMS, WhatsApp, Viber, Telegram</small>
                  <ul class="message-list">
                    <li><span class="sprite sprite-sms"></span></li>
                    <li><span class="sprite sprite-whatsapp"></span></li>
                    <li><span class="sprite sprite-viber"></span></li>
                    <li><span class="sprite sprite-telegram"></span></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-sm-6 col-md-pull-3 col-md-3">
              <div class="info-block address-block"><i class="sprite sprite-compass"></i>
                <div class="info-text">
                  <?php echo $options['address']; ?>
                  <br><a href="/contacts/">посмотреть на карте</a>
                </div>
                <button class="btn btn-callback" data-toggle="modal" href="#contact_modal">Написать нам</button>
              </div>
            </div>

            <div class="col-sm-6 col-md-pull-3 col-md-3">
              <div class="info-block phone-block"><i class="sprite sprite-headphones"></i>
                <div class="info-text">
                  <a href="tel:<?php echo $options['phone']; ?>" class="phone-link"><?php echo $options['phone']; ?></a>
                  <small><?php echo $options['worktime']; ?></small>
                </div>
                <button class="btn btn-callback" data-toggle="modal" href='#callback_modal'>Заказать звонок</button>
              </div>
            </div>


      </div><!-- /.header-top -->

      <div class="navbar navbar-default">

        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#" class="navbar-brand visible-xs"><img src="<?php echo get_template_directory_uri() ?>/assets//img/logo.png"></a>
        </div>

        <?php
          wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
          );
        ?>
      </div><!-- /.navbar -->

    </div><!-- /.container -->
  </header><!-- /#header -->

  <div class="wrapper">