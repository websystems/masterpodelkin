<?php
/**
 * The template for 404 page.
 *
 * @package masterpodelkin1
 */
  get_header();
 ?>

<div class="main-container">

  <div class="container">

    <?php get_sidebar('left') ?>


    <main class="main-section error-section">
      <?php the_post(); ?>
      <h1>404</h1>
      <h3 class="page-title">Страница не найдена</h3>
      <a href="<?php echo home_url() ?>">вернуться на главную</a>
      <div class="page-content">
        <?php the_content(); ?>
      </div>

    </main><!--/.main-section -->


    <?php get_sidebar('right') ?>

  </div><!-- /.container -->
</div><!-- /.main-container -->

<?php get_footer(); ?>