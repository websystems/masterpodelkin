var gulp = require('gulp');
var stylus = require('gulp-stylus');
var nib = require('nib');
var minifyCSS = require('gulp-minify-css');
var rename = require("gulp-rename");
var livereload = require('gulp-livereload');

var path = './assets/';

gulp.task('stylus', function() {
  gulp.src(path + 'styl/**/*.styl')
    .pipe(stylus({use: nib()}))
    .on('error', console.log)
    .pipe(gulp.dest(path + 'css/'))
    .pipe(minifyCSS({keepBreaks:true}))
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest(path + 'css/'))
    .pipe(livereload());
});

gulp.task('js', function() {
  gulp.src(path + 'js/**/*.js')
    .pipe(livereload());
});

gulp.task('watch', function(){
  livereload.listen();
  gulp.watch(path + 'styl/**/*.styl',['stylus']);
  gulp.watch(path + 'js/**/*.js',['js']);
});

gulp.task('default',['watch','stylus','js']);