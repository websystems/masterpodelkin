<?php
/**
 * Template Name: Главная
 *
 * @package masterpodelkin1
 */
  get_header();
 ?>

<div class="main-container">

  <div class="container">

    <?php get_sidebar('left') ?>


    <main class="main-section">
      <div class="page-content">
        <?php the_post(); the_content(); ?>
      </div>

      <h3 class="page-title">Лента событий</h3>
      <div class="news-section">
        <?php
          $query_args = array(
            'post_type' => 'post',
            'posts_per_page' => 5,
            'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
            );
          $wpex_query = new WP_Query( $query_args );
        ?>

        <?php if ( $wpex_query->have_posts() ) : ?>

          <?php while ( $wpex_query->have_posts() ) : $wpex_query->the_post();  ?>
            <?php get_template_part('templates/content', get_post_format()); ?>

          <?php endwhile; ?>

          <?php numeric_posts_nav('home/'); ?>

          <?php else : ?>
          <p><?php _e('Извините, записей пока нет.', 'masterpodelkin1'); ?></p>
        <?php endif; ?>

      </div><!-- /.news-section -->


    </main><!--/.main-section -->


    <?php get_sidebar('right') ?>

  </div><!-- /.container -->
</div><!-- /.main-container -->

<?php get_footer(); ?>