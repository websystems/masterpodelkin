<aside class="sidebar-left">
  <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Left sidebar') ) : ?>
  <?php endif; ?>

  <?php

  $taxonomy = 'master_classes_categories';
  $terms = get_terms($taxonomy, array('orderby' =>  'term_order')); // Get all terms of a taxonomy

  // if there are master-classes
  if ( $terms && !is_wp_error( $terms ) ) :
  ?>
    <div class="categories">

      <?php
      // each master-class loop
      foreach ( $terms as $term ):
      ?>
        <div class="category-section">
          <h2><?php echo $term->name; ?>:</h2>
          <?php

          $args = array(
            'post_type' => 'master_klasses',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'tax_query' => array(
              array(
                'taxonomy' => 'master_classes_categories',
                'field' => 'slug',
                'terms' => $term->slug
              )
            )
          );
          $master_klasses = new WP_Query( $args );
          if( $master_klasses->have_posts() ): ?>
            <ul>
            <?php
            while( $master_klasses->have_posts() ):
              $master_klasses->the_post();
              ?>
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endwhile; ?>
            </ul>
            <a href="#" class="more-categories">
              <span class="text">показать все</span>
              <span class="arr">&darr;</span>
            </a>
          <?php endif; ?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif;?>



  <div class="banner-rectangle">Хочешь интересно провести время?<br>Мастер-классы в студии&nbsp;<a href="http://podelkinclub.ru">Podelkinclub.ru</a></div>
</aside><!-- /.sidebar-left -->