<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'masterpodelkin_wp');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vt+Nvz--[mv!owC%qe@0kOdDpb:o%}k|rL^HC>qw?1V6e2!^DA|]M^+gP>Ib i5W');
define('SECURE_AUTH_KEY',  '#%~G bG^|2AzS>U`-@gyjl_TirV%.S$B]M8aw|kw1:+n2Ui%+s;p)hjJ&N9*/ nw');
define('LOGGED_IN_KEY',    'y$dm giQ`pL#BGIaZi]p%1oM&[EQPh--#g)99pq)ndy+D64mCcGgGNRg?)#Ls!a*');
define('NONCE_KEY',        'GR;S[,,OnBJM|yksafbL(fNi2d!ghUQRhTi:ZquS(D~(z{e|1Bl;!fmV!YHB|q_U');
define('AUTH_SALT',        '%L91n/tCH(9s-4e:5.,K%59!NM( y0xpMxW-Kz9REI9/@Bg[F2{9h@K?<e?,y<#.');
define('SECURE_AUTH_SALT', '|H&7{.&E#;L3E>Lj/duBveN<OT8w2bc{11xKmU[:B;&]6y^lFa:3,z+Dxo/r{QM}');
define('LOGGED_IN_SALT',   '%J}QSjX%FP|ran`awtUgVVerRxq`|i5i2S<}kNVFP@4Te%n0hyT|$)rqqP%l+))x');
define('NONCE_SALT',       'B(#acU|0}[Kf@pN*!5MwfE;)VXxf_+)#dHa<9o:aSw>E@9A!I{xBFiIAw{ot>rk ');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_masterpodelkin_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
